import 'dart:convert';
import 'dart:core';
import 'package:correfulles_app/domain/album.dart';
import 'package:http_parser/http_parser.dart';
import 'package:http/http.dart' as http;


const String CORREFULLES_API_URL = "192.168.1.44:5000";
const String ALBUMS_ACTION = "/albums/";
const String IDENTIFICADOR_ACTION = "/identificar/";

class CorreFullesApi {
  CorreFullesApi();

  Future<List<Album>> obteAlbumsDeCiutat(String nomCiutat) async {
    try {
      final queryParams = {'ciutat': nomCiutat.toLowerCase()};
      Uri url = Uri.http(CORREFULLES_API_URL, ALBUMS_ACTION, queryParams);
      print("Parlant amb API");
      String cosResposta = await http.get(url).then((resposta) {

        if (resposta.statusCode == 200) {
          print("Albums descarregats");
          return utf8.decode(latin1.encode(resposta.body),
              allowMalformed: true);
        } else {
          return "Error: (${resposta.statusCode}) ${resposta.reasonPhrase}";
        }
      }, onError: (e) => "Error: ${e.toString()}");

      print("Resposta decodificada");

      if (cosResposta.startsWith('Error', 0)) {
        throw Exception(cosResposta);
      }
      final Map<String, dynamic> respostaJsonMap = jsonDecode(cosResposta);
      List<Album> procesaResposta(Map<String, dynamic> cosResposta) {
        final List<Map<String, dynamic>> elementsAlbums =
            List<Map<String, dynamic>>.from(cosResposta['albums']);
        print("Iniciem la jsonificacio dels resultats");
        return elementsAlbums
            .map<Album>((jsonAlbum) => Album.fromJson(jsonAlbum))
            .toList();
      }

      return procesaResposta(respostaJsonMap);
    } catch (error) {
      throw Exception(error.toString());
    }
  }

  static Future<dynamic> errorApi(String missatge) {
    return Future.error(missatge);
  }

  Future<List<String>> obtenirEspeciesPossibles(String rutaFoto) async {
    var uri = Uri.https(CORREFULLES_API_URL, IDENTIFICADOR_ACTION);
    var request = http.MultipartRequest("POST", uri);
    request.files.add(await http.MultipartFile.fromPath(
      'foto_planta',
      rutaFoto,
      contentType: MediaType('application', 'x-tar'),
    ));
    var resposta = await request.send();
    if (resposta.statusCode == 400) {
      errorApi('No es troba el servidor');
    } else if (resposta.statusCode != 200) {
      errorApi(
          'Error ${resposta.statusCode} :\n ${resposta.stream.bytesToString()}');
    }
    Map<String, dynamic> jsonResposta =
        jsonDecode(await resposta.stream.bytesToString());
    return List<String>.from(jsonResposta['noms']);
  }
}
