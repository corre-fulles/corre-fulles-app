import 'package:correfulles_app/apis/correfulles_api.dart';
import 'package:correfulles_app/domain/album.dart';
import 'package:correfulles_app/domain/ciutat.dart';
import 'package:correfulles_app/domain/repository.dart';
import 'package:correfulles_app/serveis/localitzador.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'estats.dart';
import 'events.dart';

class AlbumBloc extends Bloc<AlbumsEvent, AlbumsState> {
  final CorreFullesApi correfullesService = CorreFullesApi();
  final AlbumRepository albumRepository;

  AlbumBloc({required this.albumRepository}) : super(CarregantAlbums()) {
    on<ObtenirAlbumsCiutat>((event, emit) async {
      emit(CarregantAlbums());
      print("Inici");

      try {
        if (await albumRepository.totalAlbumsACiutat(event.ciutat) == 0) {
          print("Solicitar albums");
          final List<Album> albumsCiutatDeApi =
              await correfullesService.obteAlbumsDeCiutat(event.ciutat.nom);
          await albumRepository.afegirMolts(albumsCiutatDeApi, event.ciutat);
        } else {
          print("Els albums ja estan presents a la BD local");
        }
        print("Carregant albums des de la BD");
        List<Album> albumsCiutat =
            await albumRepository.perCiutat(event.ciutat);
        print("Peticio completada");
        emit(AlbumsCarregats(albums: albumsCiutat));
      } catch (e) {
        emit(AlbumsError(message: e.toString()));
      }
    });
  }
}

class IdentificacioBloc extends Bloc<IdentificacioEvent, IdentificacioState> {
  final CorreFullesApi correfullesService = CorreFullesApi();

  IdentificacioBloc() : super(IdentificacioIdle()) {
    on<IdentificarPlantaAFoto>((event, emit) async {
      emit(IdentificacioPendent());
      try {
        final List<String> candidats = await correfullesService
            .obtenirEspeciesPossibles(event.rutaProvisionalFoto);
        emit(IdentificacioFinalitzada(nomsCientificsCandidats: candidats));
      } catch (e) {
        emit(IdentificacioError(message: e.toString()));
      }
    });
  }
}

class LocalitzacioBloc extends Bloc<LocalitzacioEvent, LocalitzacioState> {
  final Localitzador geoservice = Localitzador();
  final CiutatRepository ciutatRepository;

  LocalitzacioBloc({required this.ciutatRepository})
      : super(LocalitzacioIdle()) {
    on<Localitzar>((event, emit) async {
      emit(LocalitzacioPendent());
      try {
        print("Geolocalitzant");
        Ciutat c = await geoservice.localitza();
        print("Geolocalitzacio completada");
        if (!await ciutatRepository.existeix(c)) {
          print("Nova ciutat per la BD");
          c.id = await ciutatRepository.afegir(c);
          print("Ciutat afegida");
        } else {
          print("Carregant ciutat");
          c = await ciutatRepository.perNom(c.nom);
          print("Ciutat carregada");
        }
        emit(LocalitzacioFinalitzada(ciutat: c));
      } catch (e) {
        emit(LocalitzacioError(message: e.toString()));
      }
    });
  }
}
