import 'dart:io';

import 'package:correfulles_app/domain/album.dart';
import 'package:correfulles_app/domain/ciutat.dart';
import 'package:correfulles_app/domain/entrada.dart';



abstract class CorreFullesState {}

abstract class AlbumsState extends CorreFullesState {}

class CarregantAlbums extends AlbumsState {}

class AlbumsCarregats extends AlbumsState {
  final List<Album> albums;

  AlbumsCarregats({required this.albums});
}

class AlbumsError extends AlbumsState {
  final String message;

  AlbumsError({required this.message});
}

abstract class CamaraState extends CorreFullesState {

}

class CamaraPendentFoto extends CamaraState{}


class CamaraFotoCreada extends CamaraState {
  final File arxiuFoto;
  CamaraFotoCreada({required this.arxiuFoto});
}


abstract class EntradaState extends CorreFullesState {

}

class EntradaIdle extends EntradaState {

}

class EntradaActualitzada extends EntradaState {
  final Entrada novaEntrada;
  EntradaActualitzada({required this.novaEntrada});
}

class EntradaError extends EntradaState {
  final String message;
  EntradaError({required this.message});

}


abstract class IdentificacioState extends CorreFullesState {

}
class IdentificacioIdle extends IdentificacioState {}
class IdentificacioPendent extends IdentificacioState{}

class IdentificacioFinalitzada extends IdentificacioState{
  List<String> nomsCientificsCandidats;
  IdentificacioFinalitzada({this.nomsCientificsCandidats = const []});

}

class IdentificacioAcceptada extends IdentificacioState{
  String nomCientificSeleccionat;
  IdentificacioAcceptada({required this.nomCientificSeleccionat});
}

class IdentificacioCancelada extends IdentificacioState{}

class IdentificacioError extends IdentificacioState {
  final String message;
  IdentificacioError({required this.message});

}

abstract class LocalitzacioState extends CorreFullesState{}

class LocalitzacioPendent extends LocalitzacioState {}
class LocalitzacioFinalitzada extends LocalitzacioState {
  final Ciutat ciutat;
  LocalitzacioFinalitzada({required this.ciutat});
}
class LocalitzacioError extends LocalitzacioState {
  final String message;
  LocalitzacioError({required this.message});
}
class LocalitzacioIdle extends LocalitzacioState {}