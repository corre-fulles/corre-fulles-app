import 'dart:io';

import 'package:correfulles_app/bloc/estats.dart';
import 'package:correfulles_app/domain/album.dart';
import 'package:correfulles_app/domain/ciutat.dart';
import 'package:correfulles_app/domain/entrada.dart';
import 'package:correfulles_app/domain/espai_verd.dart';
import 'package:flutter/material.dart';

abstract class CorreFullesEvent {}

abstract class AlbumsEvent extends CorreFullesEvent {}

abstract class IdentificacioEvent extends CorreFullesEvent {}

abstract class EntradaEvent extends CorreFullesEvent {}

abstract class LocalitzacioEvent extends CorreFullesEvent {}

class ObtenirAlbumsCiutat extends AlbumsEvent {
  late Ciutat ciutat;
  ObtenirAlbumsCiutat({required this.ciutat});
}

class ActualitzarEntradaAlbum extends AlbumsEvent {
  late Entrada entrada_actualitzada;
  late Album album;

}


class IdentificarPlantaAFoto extends IdentificacioEvent {
  late String rutaProvisionalFoto;
  late EspaiVerd espai;
  IdentificarPlantaAFoto({required this.rutaProvisionalFoto});
}

class AcceptarIdentificacioPlanta extends IdentificacioEvent {
  late String rutaProvisionalFoto;
  late String nomCientificEscollit;
  AcceptarIdentificacioPlanta({required this.nomCientificEscollit});
}

class CancelarIdentificacioPlanta extends IdentificacioEvent {
  late String rutaProvisionalFoto;
}


class ActualitzarEntrada extends EntradaEvent {
  late Entrada entrada;
  late String nom_ciutat;
  late String nom_espai;
  late String ruta_foto;
  ActualitzarEntrada({required this.entrada, required this.ruta_foto, required this.nom_ciutat, required this.nom_espai});
}

class Localitzar extends LocalitzacioEvent{}
class DenegarPermisLocalitzacio extends LocalitzacioEvent{}


