import 'dart:io';

import 'package:correfulles_app/domain/entrada.dart';
import 'package:flutter/material.dart';

class EntradaWidget extends StatelessWidget {
  final Entrada element;

  const EntradaWidget({super.key, required this.element});

  @override
  Widget build(BuildContext context) {
    final screen = MediaQuery.of(context).size;
    Image imatgeEntrada = element.esLliure
        ? Image.asset(
            'assets/images/default/no_plant.jpg',
            fit: BoxFit.fill,
            width: 80.0,
            height: 80.0,
          )
        : Image.file(
            File(element.foto!.rutaArxiu),
            fit: BoxFit.fill,
            width: 100.0,
            height: 105.0,
          );

    element.esLliure
        ? 'assets/images/default/no_plant.jpg'
        : element.foto!.rutaArxiu;
    double opacitatEntrada = element.esLliure ? 0.8 : 1.0;
    return Opacity(
        opacity: opacitatEntrada,
        child: Material(
          child: SizedBox(
            height: screen.height / 30,
            child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)),
              child: SizedBox(height: 500, width: 300, child: Center(child: Column(
                children: [
                  imatgeEntrada,

                  Text(element.planta.nomCientific,style: TextStyle(fontSize: 12)),
                  Text("Nom comú", style:TextStyle(fontSize: 10)),
                  Text("\t[CA]: ${element.planta.nomComu.nomCatala()}", style:TextStyle(fontSize: 8)),
                  Text("\t[ES]: ${element.planta.nomComu.nomCastella()}", style:TextStyle(fontSize: 8)),
                  // Text("\t[EN]: ${element.planta.nomComu.nomAngles()}"),
                ],
              )),),
            ),
          ),
        )
    );
  }
}
