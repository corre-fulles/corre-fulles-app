import 'dart:io';

import 'package:correfulles_app/domain/album.dart';
import 'package:correfulles_app/serveis/AppPathProvider.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as p;
import 'entrada_grid_item.dart';

class AlbumShow extends StatefulWidget {
  Album album;

  AlbumShow({
    Key? key,
    required this.album,
  }) : super(key: key);

  @override
  _AlbumShowState createState() => _AlbumShowState();
}

class _AlbumShowState extends State<AlbumShow> {
  File? novaImatge;
  final ImagePicker _picker = ImagePicker();
  dynamic _pickImageError;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    novaImatge = null;
  }

  @override
  void deactivate() {
    super.deactivate();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<String?> guardaImatge(XFile? arxiu) async {
    if (arxiu != null) {
      Directory temporal = AppPathProvider.instance.carpetaDadesUsuari;
      String nomDirAlbum =
          widget.album.espai.nom.toLowerCase().replaceAll(' ', '');
      Directory temporalAlbum =
          Directory(p.join(temporal.path, nomDirAlbum, 'provisionals'));
      String rutaFinal = p.join(temporalAlbum.path, '${arxiu.name}.png');
      if (!await temporalAlbum.exists()) {
        await temporalAlbum.create(recursive: true);
      }
      await arxiu.saveTo(rutaFinal);
      return rutaFinal;
    } else {
      return null;
    }
  }

  Future<XFile?> capturaFoto(
      double? maxWidth, double? maxHeight, int? quality) async {
    final XFile? pickedFile = await _picker.pickImage(
      source: ImageSource.camera,
      maxWidth: maxWidth,
      maxHeight: maxHeight,
      imageQuality: quality,
    );
    return pickedFile;
  }

  Future<void> _onImageButtonPressed() async {
    try {
      XFile? resultat = await capturaFoto(100, 100, 100);
      if (resultat != null) {
        setState(() {
          _setImageFileFromFile(resultat);
        });
      }
    } catch (e) {
      setState(() {
        _pickImageError = e;
      });
    }
  }

  Future<void> _setImageFileFromFile(XFile? pickedFile) async {
    String? ruta = await guardaImatge(pickedFile);
    if (ruta != null) {
      novaImatge = File(ruta);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.album.espai.nom),
      ),
      body: GraellaAlbumWidget(album: widget.album),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: FloatingActionButton(
              onPressed: () {
                _onImageButtonPressed();
              },
              heroTag: 'image2',
              tooltip: 'Take a Photo',
              child: const Icon(Icons.camera_alt),
            ),
          ),
          Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: FloatingActionButton(
                onPressed: () => showDialog(
                  context: context,
                  builder: (BuildContext context) => AlertDialog(
                    title: const Text('AlertDialog Title'),
                    content: novaImatge != null
                        ? Image.file(novaImatge!)
                        : Text('Nothing yet'),
                    actions: <Widget>[
                      TextButton(
                        onPressed: () => Navigator.pop(context, 'Cancel'),
                        child: const Text('Cancel'),
                      ),
                      TextButton(
                        onPressed: () => Navigator.pop(context, 'OK'),
                        child: const Text('OK'),
                      ),
                    ],
                  ),
                ),
                child: const Icon(Icons.account_box_outlined),
              ))
        ],
      ),
    );
  }
}

class GraellaAlbumWidget extends StatelessWidget {
  const GraellaAlbumWidget({
    Key? key,
    required this.album,
  }) : super(key: key);

  final Album album;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(1.0),
      child: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, mainAxisSpacing: 1.0),
        padding: EdgeInsets.symmetric(
          horizontal: 27.0,
        ),
        itemBuilder: (context, index) =>
            EntradaWidget(element: album.entrades[index]),
        itemCount: album.entrades.length,
      ),
    );
  }
}
