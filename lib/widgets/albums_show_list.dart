import 'package:correfulles_app/bloc/bloc.dart';
import 'package:correfulles_app/bloc/estats.dart';
import 'package:correfulles_app/domain/album.dart';
import 'package:correfulles_app/widgets/album_show_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AlbumsShowList extends StatefulWidget {
  @override
  _AlbumsShowListState createState() => _AlbumsShowListState();
}

class _AlbumsShowListState extends State<AlbumsShowList> {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AlbumBloc, AlbumsState>(
      listener: (context, state) {
        if (state is AlbumsError) {
          ScaffoldMessenger.of(context)
              .showSnackBar(SnackBar(content: Text(state.message)));
        }
      },
      builder: (context, state) {
        print(state.runtimeType);
        if (state is CarregantAlbums) {
          return const Center(child: CircularProgressIndicator());
        }
        if (state is AlbumsCarregats) {
          return LlistaAlbumsWidget(albums: state.albums);
        } else if (state is AlbumsError) {
          throw StateError('Estat desconegut: ${state.message}');
        } else {
          throw UnimplementedError(
              "Unimplemeted method for state ${state.runtimeType}");
        }
      },
    );
  }
}

class LlistaAlbumsWidget extends StatelessWidget {
  const LlistaAlbumsWidget({
    Key? key,
    required this.albums,
  }) : super(key: key);

  final List<Album> albums;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: albums.length,
      itemBuilder: (BuildContext context, int index) {
        int numeroPlantes = albums[index].entrades.length;
        bool ambPlantes = numeroPlantes > 0;
        String titol =
            "${albums[index].espai.nom} (${numeroPlantes})";
        return ListTile(
          title: Text(titol),
          onTap: () {
            if (ambPlantes) {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          AlbumShow(album: albums[index])));
            }
          },
        );
      },
    );
  }
}
