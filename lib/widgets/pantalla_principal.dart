import 'package:correfulles_app/domain/ciutat.dart';
import 'package:correfulles_app/widgets/pantalla_albums.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:correfulles_app/bloc/bloc.dart';
import 'package:correfulles_app/bloc/estats.dart';
import 'package:correfulles_app/bloc/events.dart';
import 'package:permission_handler/permission_handler.dart';

class PantallaLocalitzacio extends StatefulWidget {
  @override
  _PantallaLocalitzacioState createState() => _PantallaLocalitzacioState();
}

class _PantallaLocalitzacioState extends State<PantallaLocalitzacio> {
  @override
  Widget build(BuildContext context) {
    final locationBloc = BlocProvider.of<LocalitzacioBloc>(context);
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("Localitzador"),
        ),
        body: BlocConsumer<LocalitzacioBloc, LocalitzacioState>(
          listener: (context, state) {
            if (state is LocalitzacioError) {
              ScaffoldMessenger.of(context)
                  .showSnackBar(SnackBar(content: Text(state.message)));
            }
          },
          builder: (context, state) {
            print(state.runtimeType);
            if (state is LocalitzacioIdle) {
              return Center(child: Text("Encara no se a quina ciutat ens trobem"));
            }
            if (state is LocalitzacioPendent) {
              return Center(child: CircularProgressIndicator());
            }
            if (state is LocalitzacioFinalitzada) {
              final  Ciutat ciutat_actual = state.ciutat;
              return Center(child: Column(children: [
                Text("Ens trobem a ${state.ciutat.nom}"),
                ElevatedButton(
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => PantallaLlistaAlbums(ciutat:ciutat_actual)
                    )
                    );
                    // Navigate back to first route when tapped.
                  },
                  child: Text('Albums per ${state.ciutat.nom}'),
                )
              ],),);
            } else if (state is LocalitzacioError) {
              throw StateError('Error: ${state.message}');
            } else {
              throw UnimplementedError(
                  "Unimplemeted method for state ${state.runtimeType}");
            }
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            if (await Permission.location.request().isGranted) {
            print("GPS premut");
            locationBloc.add(Localitzar());
            // Either the permission was already granted before or the user just granted it.
            }

          },
          child: Icon(Icons.gps_fixed_rounded),
        ));

  }


}