import 'package:correfulles_app/bloc/bloc.dart';
import 'package:correfulles_app/bloc/events.dart';
import 'package:correfulles_app/domain/ciutat.dart';
import 'package:correfulles_app/widgets/albums_show_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PantallaLlistaAlbums extends StatelessWidget {
  final Ciutat ciutat;
  const PantallaLlistaAlbums({super.key, required this.ciutat });
  @override
  Widget build(BuildContext context) {
    final albumsBloc = BlocProvider.of<AlbumBloc>(context);
    return Scaffold(
          appBar: AppBar(
            title: Text("Espais verds de ${ciutat.nom}"),
          ),
          body: AlbumsShowList(),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              print("Boto premut");
              albumsBloc.add(ObtenirAlbumsCiutat(ciutat: ciutat));
            },
            child: Icon(Icons.refresh),
          ),
    );
  }

}