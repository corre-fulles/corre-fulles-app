import 'package:correfulles_app/bloc/bloc.dart';
import 'package:correfulles_app/infrastructure/objectbox/album/album_repository.dart';
import 'package:correfulles_app/infrastructure/objectbox/ciutat/ciutat_repository.dart';
import 'package:correfulles_app/infrastructure/objectbox/driver.dart';
import 'package:correfulles_app/serveis/AppPathProvider.dart';
import 'package:correfulles_app/widgets/pantalla_principal.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:splashscreen/splashscreen.dart';

late final ObjectBoxDriver objectBoxDriver;
late final AppPathProvider appPathProvider;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  if (
  await Permission.camera.request().isGranted &
  await Permission.storage.request().isGranted ) {
    objectBoxDriver = ObjectBoxDriver.instance;
    appPathProvider = AppPathProvider.instance;

    await appPathProvider.inicialitza();
    await objectBoxDriver.inicialitza();

    runApp(MyApp());
  }
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<LocalitzacioBloc>(
              create: (context) => LocalitzacioBloc(
                  ciutatRepository: CiutatObjectBoxRepository(
                      obmanager: objectBoxDriver.boxCiutats))),
          BlocProvider<AlbumBloc>(
              create: (context) => AlbumBloc(
                  albumRepository: AlbumObjectBoxRepository(
                      albumsOBManager: objectBoxDriver.boxAlbums,
                      ciutatsOBManager: objectBoxDriver.boxCiutats,
                    plantesOBManager: objectBoxDriver.boxPlantes

                  )))
        ],
        child: MaterialApp(
            // Application name
            title: 'Correfulles',
            theme: ThemeData(
              // Application theme data, you can set the colors for the application as
              // you want
              primarySwatch: Colors.green,
            ),
            home: PantallaBenvinguda()));
  }
}

class PantallaBenvinguda extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 6,
      navigateAfterSeconds: CorrefullesMainPage(),
      backgroundColor: Colors.deepOrange.shade200,
      title: Text(
        'Benvingut',
        textScaleFactor: 2,
      ),
      image: Image.asset('assets/logo_main.png'),
      loadingText: Text("Loading"),
      photoSize: 150.0,
      loaderColor: Colors.green,
    );
  }
}

class CorrefullesMainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return PantallaLocalitzacio();
  }
}
