import 'package:correfulles_app/domain/ciutat.dart';
import 'package:correfulles_app/domain/repository.dart';
import 'package:correfulles_app/infrastructure/objectbox/domain/models.dart';
import 'package:correfulles_app/objectbox.g.dart';


class CiutatObjectBoxRepository implements CiutatRepository {
  final Box<OBCiutat> obmanager;

  CiutatObjectBoxRepository({required this.obmanager});

  Future<int> afegir(Ciutat ciutat) async {
    final OBCiutat obCiutat = OBCiutat.fromDomainEntity(ciutat);
    return obmanager.put(obCiutat);
  }

  Future<Ciutat> perId(int id) async {
    return obmanager.get(id)!.toDomainEntity();
  }

  Future<Ciutat> perNom(String nom) async {
    return obmanager
        .query(OBCiutat_.nom.equals(nom))
        .build()
        .findUnique()!
        .toDomainEntity();
  }

  Future<bool> existeix(Ciutat ciutat) async {
    QueryBuilder<OBCiutat> queryBuilder =
        obmanager.query(OBCiutat_.nom.equals(ciutat.nom));
    final query = queryBuilder.build();
    final bool hiHaCiutat = query.findUnique() != null;
    query.close();
    return hiHaCiutat;
  }

  Future<bool> teAlbums(Ciutat ciutat) async {
    OBCiutat obCiutat = obmanager.get(ciutat.id)!;
    return obCiutat.espais.isNotEmpty;
  }
}
