import 'package:correfulles_app/domain/album.dart';
import 'package:correfulles_app/domain/ciutat.dart';
import 'package:correfulles_app/domain/entrada.dart';
import 'package:correfulles_app/domain/espai_verd.dart';
import 'package:correfulles_app/domain/foto.dart';
import 'package:correfulles_app/domain/planta.dart';
import 'package:objectbox/objectbox.dart';

@Entity()
class OBCiutat {
  @Id()
  int id = 0;
  @Unique()
  String nom;
  @Backlink()
  final espais = ToMany<OBEspaiVerd>();

  OBCiutat({required this.nom});

  factory OBCiutat.fromDomainEntity(Ciutat ciutat) {
    OBCiutat obCiutat = OBCiutat(nom: ciutat.nom);
    obCiutat.id = ciutat.id;
    return obCiutat;
  }

  Ciutat toDomainEntity() {
    Ciutat ciutat = Ciutat(nom: nom);
    ciutat.id = id;
    return ciutat;
  }
}

@Entity()
class OBAlbum {
  @Id()
  int id = 0;
  final espai = ToOne<OBEspaiVerd>();
  @Backlink()
  final entrades = ToMany<OBEntradaAlbum>();

  OBAlbum();

  Album toDomainEntity() {
    Album album = Album(espai.target!.toDomainEntity(),
        entrades.map<Entrada>((element) => element.toDomainEntity()).toList());
    album.id = id;
    return album;
  }


  factory OBAlbum.fromDomainEntity(Album album) {
    OBAlbum obAlbum = OBAlbum();
    obAlbum.id = album.id;
    OBEspaiVerd obEspaiVerd = OBEspaiVerd.fromDomainEntity(album.espai);
    obAlbum.espai.target = obEspaiVerd;
    album.entrades.forEach((element) {
      OBEntradaAlbum obEntradaAlbum = OBEntradaAlbum();
      OBPlanta obPlanta = OBPlanta.fromDomainEntity(element.planta);
      obEntradaAlbum.planta.target = obPlanta;
      if (!element.esLliure && element.foto != null) {
        OBFoto obFoto = OBFoto.fromDomainEntity(element.foto!);
        obEntradaAlbum.foto.target = obFoto;
      }
      obAlbum.entrades.add(obEntradaAlbum);
    });
    return obAlbum;
  }
}

@Entity()
class OBEspaiVerd {
  @Id()
  int id = 0;

  @Unique()
  String nom;
  String? districte;

  final ciutat = ToOne<OBCiutat>();
  final album = ToOne<OBAlbum>();

  OBEspaiVerd({required this.nom, this.districte = ""});

  factory OBEspaiVerd.fromDomainEntity(EspaiVerd espaiVerd) {
    return OBEspaiVerd(nom: espaiVerd.nom, districte: espaiVerd.districte);
  }

  EspaiVerd toDomainEntity() {
    return EspaiVerd(nom, districte!);
  }
}

@Entity()
class OBEntradaAlbum {
  @Id()
  int id = 0;

  OBEntradaAlbum();

  final planta = ToOne<OBPlanta>();
  final foto = ToOne<OBFoto>();
  final album = ToOne<OBAlbum>();

  Entrada toDomainEntity() {
    Entrada resultat = Entrada(planta: planta.target!.toDomainEntity());
    if (foto.target != null) {
      resultat.assignaFoto(foto.target!.toDomainEntity());
    }
    return resultat;
  }

  factory OBEntradaAlbum.fromDomainEntity(Entrada entrada) {
    OBEntradaAlbum novaEntrada = OBEntradaAlbum();
    novaEntrada.planta.target = OBPlanta.fromDomainEntity(entrada.planta);
    if (!entrada.esLliure && entrada.foto != null) {
      novaEntrada.foto.target = OBFoto.fromDomainEntity(entrada.foto!);
    }
    return novaEntrada;
  }
}

@Entity()
class OBFoto {
  @Id()
  int id = 0;
  @Unique()
  String ruta;

  OBFoto({required this.ruta});

  final entrada = ToOne<OBEntradaAlbum>();

  factory OBFoto.fromDomainEntity(Foto foto) {
    return OBFoto(ruta: foto.rutaArxiu);
  }

  Foto toDomainEntity() {
    return Foto(ruta);
  }
}

@Entity()
class OBPlanta {
  @Id()
  int id = 0;
  @Unique()
  String nomCientific;

  String? nomComuCatala;
  String? nomComuCastella;
  String? nomComuAngles;

  OBPlanta(
      this.nomCientific,
      {this.nomComuCatala,
      this.nomComuCastella,
      this.nomComuAngles});

  factory OBPlanta.fromDomainEntity(Planta p) {
    return OBPlanta(
       p.nomCientific,
        nomComuCatala: p.nomComu.nomCatala(),
        nomComuCastella: p.nomComu.nomCastella(),
        nomComuAngles: p.nomComu.nomAngles());
  }

  Planta toDomainEntity() {
    return Planta(
        nomCientific,
        NomComu(traduccions: {
          'ca': nomComuCatala,
          'es': nomComuCastella,
          'en': nomComuAngles
        }));
  }
}



