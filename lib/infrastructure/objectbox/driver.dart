import 'dart:io';

import 'package:correfulles_app/infrastructure/dbdriver.dart';
import 'package:correfulles_app/infrastructure/objectbox/domain/models.dart';
import 'package:correfulles_app/objectbox.g.dart';
import 'package:correfulles_app/serveis/AppPathProvider.dart';

class ObjectBoxDriver extends DBDriver {

  ObjectBoxDriver._();
  static final ObjectBoxDriver _instance = ObjectBoxDriver._();
  static ObjectBoxDriver get instance => _instance;

  late final Store store;
  Box<OBAlbum> get boxAlbums => store.box<OBAlbum>();
  Box<OBCiutat> get boxCiutats => store.box<OBCiutat>();
  Box<OBPlanta> get boxPlantes => store.box<OBPlanta>();


  Future<void> inicialitza() async {
    if (! await Directory(AppPathProvider.instance.rutaBD('appDB')).exists()) {
      await Directory(AppPathProvider.instance.rutaBD('appDB')).create(recursive: true);
    }
    store = await openStore(directory: AppPathProvider.instance.rutaBD('appDB'));
  }
}
