import 'package:correfulles_app/domain/album.dart';
import 'package:correfulles_app/domain/ciutat.dart';
import 'package:correfulles_app/domain/repository.dart';
import 'package:correfulles_app/infrastructure/objectbox/domain/models.dart';
import 'package:correfulles_app/objectbox.g.dart';

class AlbumObjectBoxRepository implements AlbumRepository {
  final Box<OBAlbum> albumsOBManager;
  final Box<OBCiutat> ciutatsOBManager;
  final Box<OBPlanta> plantesOBManager;
  OBCiutat? ciutatActiva;

  AlbumObjectBoxRepository(
      {required this.albumsOBManager,
      required this.ciutatsOBManager,
      required this.plantesOBManager});

  @override
  Future<void> actualitza(Album album) async {
    OBAlbum obAlbum = albumsOBManager.get(album.id)!;
    obAlbum.entrades.clear();
    obAlbum.entrades.addAll(
        album.entrades.map((e) => OBEntradaAlbum.fromDomainEntity(e)).toList());
    albumsOBManager.put(obAlbum, mode: PutMode.update);
  }

  OBAlbum _creaAlbumAssignatACiutatActiva(Album item,
      {Map<String, OBPlanta>? plantesUniques}) {
    OBAlbum persistedAlbum = OBAlbum.fromDomainEntity(item);
    if (plantesUniques != null) {
      persistedAlbum.entrades.forEach((element) {
        String nomPlanta = element.planta.target!.nomCientific;
        element.planta.target = plantesUniques.putIfAbsent(
            nomPlanta,
            () =>
                plantesOBManager
                    .query(OBPlanta_.nomCientific.equals(nomPlanta))
                    .build()
                    .findUnique() ??
                element.planta.target!);
      });
    }
    persistedAlbum.espai.target!.ciutat.target = ciutatActiva;
    return persistedAlbum;
  }

  Future<void> _loadOrUpdateCiutatActiva(Ciutat ciutat) async {
    if (ciutatActiva?.nom != ciutat.nom) {
      ciutatActiva ??= ciutatsOBManager
          .query(OBCiutat_.nom.equals(ciutat.nom))
          .build()
          .findUnique()!;
    }
  }

  @override
  Future<int> afegir(Album album, Ciutat ciutat) async {
    await _loadOrUpdateCiutatActiva(ciutat);
    Map<String, OBPlanta> cachePlantes = {};
    // TODO: implement afegir
    return albumsOBManager.put(
        _creaAlbumAssignatACiutatActiva(album, plantesUniques: cachePlantes));
  }

  @override
  Future<List<Album>> perCiutat(Ciutat ciutat) async {
    print("Actualitzar ciutat activa");
    await _loadOrUpdateCiutatActiva(ciutat);
    print("Llista espais");
    List<int> idsEspais =
        ciutatActiva!.espais.map<int>((element) => element.id).toList();
    print("Llista albums");
    QueryBuilder<OBAlbum> builder = albumsOBManager
        .query();
    builder.link(OBAlbum_.espai, OBEspaiVerd_.id.oneOf(idsEspais));
    Query<OBAlbum> query = builder.build();
    print("Query creada");
    print(query.count());

    List<Album> albums = query.find()
        .map<Album>((e) => e.toDomainEntity())
        .toList();
    print("Done");
    return albums;
  }

  Future<List<int>> afegirMolts(List<Album> albums, Ciutat ciutat) async {
    print("Afegim albums a bd");
    await _loadOrUpdateCiutatActiva(ciutat);
    Map<String, OBPlanta> cachePlantes = {};
    return albumsOBManager.putMany(albums
        .map<OBAlbum>((Album element) => _creaAlbumAssignatACiutatActiva(
            element,
            plantesUniques: cachePlantes))
        .toList());
  }

  @override
  Future<Album> perNom(String nom) async {
    QueryBuilder<OBAlbum> queryBuilder = albumsOBManager.query();
    queryBuilder.link(OBAlbum_.espai, OBEspaiVerd_.nom.equals(nom));
    Query<OBAlbum> query = queryBuilder.build();
    final OBAlbum album_elem = query.findUnique()!;
    return album_elem.toDomainEntity();
  }

  Future<Album> perId(int id) async {
    return albumsOBManager.get(id)!.toDomainEntity();
  }

  @override
  Future<bool> existeix(Album album) async {
    return albumsOBManager.get(album.id) != null;
  }

  @override
  Future<int> totalAlbumsACiutat(Ciutat ciutat) async {
    QueryBuilder<OBAlbum> builder = albumsOBManager.query();
    builder.link(OBAlbum_.espai, OBEspaiVerd_.ciutat.equals(ciutat.id));
    Query<OBAlbum> query = builder.build();
    int resultat = query.count();
    query.close();
    return resultat;
  }
}
