
import 'package:correfulles_app/domain/album.dart';
import 'package:correfulles_app/domain/ciutat.dart';

abstract class EntityRepository {}

abstract class AlbumRepository extends EntityRepository {
  Future<Album> perNom(String nom);
  Future<Album> perId(int id);
  Future<List<Album>> perCiutat(Ciutat ciutat);
  Future<int> afegir(Album album, Ciutat ciutat);
  Future<List<int>> afegirMolts(List<Album> albums, Ciutat ciutat);
  Future<void> actualitza(Album album);
  Future<bool> existeix(Album album);
  Future<int> totalAlbumsACiutat(Ciutat ciutat);

}

abstract class CiutatRepository extends EntityRepository {
  Future<int> afegir(Ciutat ciutat);
  Future<Ciutat> perId(int id);
  Future<bool> existeix(Ciutat ciutat);
  Future<bool> teAlbums(Ciutat ciutat);
  Future<Ciutat> perNom(String nom);
}