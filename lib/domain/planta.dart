class NomComu {
  final Map<String, dynamic> traduccions;

  NomComu({required this.traduccions});

  String _nom(String codiIdioma, String defecte) {
    final resultat = traduccions[codiIdioma] ?? defecte;
    if (resultat == defecte || resultat.runtimeType == String) {
      return resultat;
    } else {
      return List<String>.from(resultat).join(", ");
    }
  }

  String nomCatala() {
    return _nom('ca', 'No disponible');
  }

  String nomCastella() {
    return _nom('es', 'No disponible');
  }

  String nomAngles() {
    return _nom('en', 'Unavailable');
  }

  factory NomComu.fromJson(Map<String, dynamic> json) {
    return NomComu(traduccions: json);
  }
}

class Planta {
  String nomCientific;
  NomComu nomComu;

  Planta(this.nomCientific, this.nomComu);

  factory Planta.fromJson(Map<String, dynamic> json) {
    print("Jsonificant planta");
    return Planta(json['nom_cientific'], NomComu.fromJson(json['nom_comu']));
  }
}
