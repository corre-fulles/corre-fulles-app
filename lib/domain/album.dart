
import 'package:correfulles_app/domain/espai_verd.dart';
import 'package:correfulles_app/domain/planta.dart';

import 'entrada.dart';

class Album {
  int id = 0;
  EspaiVerd espai;
  List<Entrada> entrades;
  Album(this.espai, this.entrades);

  bool completat() {
    return !entrades.any((element) => element.esLliure);
  }

  factory Album.fromJson(Map<String, dynamic> json){
    print("Album Level");
    final List<Map<String, dynamic>> llista_plantes = List<Map<String, dynamic>>.from(json['plantes']);

    return Album(
        EspaiVerd.fromJson(json['espai']),
        llista_plantes.map<Entrada>(
                (jsonPlanta) => Entrada(planta: Planta.fromJson(jsonPlanta))).toList()
    );
  }
}

