class EspaiVerd {
  String nom;
  String districte;
  EspaiVerd(this.nom, this.districte);

  factory EspaiVerd.fromJson(Map<String, dynamic> json) {
    return EspaiVerd(json['nom'], json['districte']);
  }
}
