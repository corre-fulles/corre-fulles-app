
import 'foto.dart';
import 'planta.dart';

class Entrada {
  Planta planta;
  bool esLliure = true;
  Foto? foto;

  Entrada({required this.planta});

  void assignaFoto(Foto nova_foto) {
    foto = nova_foto;
    esLliure = false;
  }


  void eliminaFoto() {
    foto = null;
    esLliure = true;
  }
}
