import 'package:correfulles_app/domain/ciutat.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoding/geocoding.dart';

class Localitzador {
  Future<Ciutat> localitza() async {
    final Ciutat ciutat = await Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.best)
        .timeout(Duration(seconds: 5))
        .then((position) async {
      try {
        final Placemark p = await placemarkFromCoordinates(
                position.latitude, position.longitude)
            .then((places) => places[0]);

        return Ciutat(nom: p.locality!);
      } catch (e) {
        return Ciutat(nom: "KO: ${e.toString()}");
      }
    });
    return ciutat;
  }
}
