
import 'dart:io';

import 'package:correfulles_app/config/config.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;



class AppPathProvider {
  static final AppPathProvider _instance = AppPathProvider._();
  late final Directory carpetaDadesUsuari;
  late final Directory carpetaDadesAplicacio;
  late final Directory carpetaDadesTemporals;
  static AppPathProvider get instance => _instance;
  AppPathProvider._();

  Future<void> inicialitza() async{
    carpetaDadesUsuari = await getApplicationDocumentsDirectory();
    carpetaDadesAplicacio = await getApplicationSupportDirectory();
    carpetaDadesTemporals = await getTemporaryDirectory();
  }

  String rutaBD(String nomBD, {SupportedDatabase tipus = SupportedDatabase.objectbox}) {
    return p.join(carpetaDadesAplicacio.path,'dbs',tipus.name,nomBD);
  }




}