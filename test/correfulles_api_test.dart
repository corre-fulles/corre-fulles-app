import 'dart:convert';

import 'package:correfulles_app/apis/correfulles_api.dart';
import 'package:correfulles_app/domain/album.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  String testInfo =
      "{\n\"albums\": [\n{\n\"espai\": {\n\"districte\": \"TestD\",\n\"nom\": \"TestEv\"\n},\n\"plantes\": [\n{\n\"nom_cientific\": \"TestSpecies\",\n\"nom_comu\": {\n\"ca\": [\n\"TestNomComuCa1\",\n\"TestNomComuCa2\"\n],\n  \"es\": [\n\"TestNomComuEs1\",\n\"TestNomComuEs2\"\n]\n}}]}]}\n";

  test('Normalizes JSON', () {
    expect(() => jsonDecode(testInfo), returnsNormally);
  });
  test('Album can be parsed', () {
    final Map<String, dynamic> jsonElement =
        jsonDecode(testInfo) as Map<String, dynamic>;

    expect(() => Album.fromJson(jsonElement['albums'][0]), returnsNormally);
  });

  test('String is correctly decoded', () {
    String text = "MontjuÃ¯c, Parc";
    String resultatEncode =
        utf8.decode(latin1.encode(text), allowMalformed: true);
    expect(resultatEncode, 'Montjuïc, Parc');
  });
}
